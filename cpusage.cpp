#include <stdio.h>
#include <stdlib.h>
#include<unistd.h>

int main(void)
{
	long double a[2][4], b[2][4], avg1, avg2,x;
	FILE *fp;
	char dump[50];
	for(int i=0;i<2;i++){
		fp = fopen("/proc/stat","r");
		fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[i][0],&a[i][1],&a[i][2],&a[i][3]);
		fgets(dump, 50, fp);
		fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[i][0],&b[i][1],&b[i][2],&b[i][3]);
		fclose(fp);
		sleep(2);
	}

	avg1 = ((a[1][0]+a[1][1]+a[1][2]) - (a[0][0]+a[0][1]+a[0][2])) / ((a[1][0]+a[1][1]+a[1][2]+a[1][3]) - (a[0][0]+a[0][1]+a[0][2]+a[0][3]));

	avg2 = ((b[1][0]+b[1][1]+b[1][2]) - (b[0][0]+b[0][1]+b[0][2])) / ((b[1][0]+b[1][1]+b[1][2]+b[1][3]) - (b[0][0]+b[0][1]+b[0][2]+b[0][3]));

	printf("The CPU utilization is : %Lf\n",avg1*100);
	printf("The CPU0 utilization is : %Lf\n",avg2*100);

	return(0);
}
